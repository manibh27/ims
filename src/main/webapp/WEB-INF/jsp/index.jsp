<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html> 
    <head>
        <meta charset="UTF-8">
        <title>Create Candidate</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/css/index.css">
    </head> 
    <body>
        <div>
            <h2 align="center">Interview Schedule Management</h2>
        </div>
        <div class="sidebar">
            <a href="viewCandidates">Recruiter</a>
            <a href="schedulesByStatus">Manager</a>
            <a href="newSchedules?id=1">Employee</a>
        </div>
    <form action="viewCandidates" method="get">
        <input class="save" type="submit" value="view candidates"/>
    </form>
    <form action="getRecruiterOperations" method="get">
        <input class="save" type="submit" value="Recruiter"/>
    </form>
</body>
</html>
