package com.ideas2it.ism.service;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import com.ideas2it.ism.common.ScheduleStatus;
import com.ideas2it.ism.entity.Employee;
import com.ideas2it.ism.entity.Schedule;
import com.ideas2it.ism.entity.ScheduleRejectionTrack;
import com.ideas2it.ism.exception.IsmException;

public interface ScheduleService {

	/**
	 * Adds the schedule for a candidate which is generated by the recruiter
	 * 
	 * @param schedule - Schedule created by the recruiter
	 * @param candidateId - Id of the candidate for whom the schedule is created.
	 * @param date - interview date which is in string format
	 * @param time - interview time which is in string format
	 * 
	 * @return true when the schedule added successfully and id created for that schedule else false
	 */

	Schedule addSchedule(Schedule schedule, long candidateId, String date, String time);
	
	/**
	 * For the given candidate id the schedules conducted for the particular candidate
	 * is fetch and returned as list.
	 * 
	 * @param candidateId - Id of the candidate whose schedule track is required.
	 * 
	 * @return schedules - List of schedules conducted for the particular candidate.
	 * If there is no schedule conducted empty list is passed.
	 */
	List<Schedule> fetchSchedulesByCandidateId(long candidateId);
	
	/**
	 * For the given employee id the newly assigned schedules for the particular employee
	 * is fetch and returned as list.
	 * 
	 * @param employeeId - Id of the employee whose newly assigned schedule to be fetched.
	 * @return schedules - List of newly assigned schedules for the particular employee.
	 * If there is no schedule is assigned empty list is passed.
	 */
	List<Schedule> getEmployeeNewSchedulesById(long employeeId);

	/**
	 * For the given employee id the pending schedules for the particular employee
	 * is fetch and returned as list.
	 * 
	 * @param employeeId - Id of the employee whose newly assigned schedule to be fetched.
	 * @return schedules - List of pending schedules for the particular employee.
	 * If there is no schedule is assigned empty list is passed.
	 */
	List<Schedule> getEmployeePendingSchedulesById(long employeeId);

	/**
	 * The schedule is fetched from DB and the given status is updated.
	 * 
	 * @param scheduleId - Id of the schedule in which the status to be changed.
	 * @param status - Type of status to be updated.
	 */
	void updateScheduleStatus(long scheduleId, ScheduleStatus status);
	
	/**
	 * When the employee updates the result of the schedule id, feedback and result
	 * is obtained and updated in the schedule.
	 * 
	 * @param feedBack - Comments passed for the interview result.
	 * @param scheduleId - Id of the schedule in which the result to be updated.
	 * @param result - Result of the schedule.
	 */
	void updateResult(String feedBack, long scheduleId, String result);
	
	/**
	 * Gets all schedules
	 * 
	 * @return schedules - List of all schedules.
	 */
	List<Schedule> getAllSchedules();
	
	/**
	 * Gets the schedule having the given ID
	 * 
	 * @param id - Schedule ID which should not be null.
	 * 
	 * @return schedule - If the id exists, the schedule will be returned else null will be returned
	 */
	Schedule getScheduleById(long id);
	
	/**
	 * Updates the schedule status as cancelled.
	 * 
	 * @param scheduleInfo - Model schedule from the client along with cancellation comment
	 * @param comment - Comment for cancelling
	 * 
	 * @return true if the status of the schedule is updated as cancelled else false
	 */
	boolean cancelSchedule(Schedule scheduleInfo, String comment);
	
	/**
	 * Updates the schedule status as rescheduled and also creates a new schedule.
	 * 
	 * @param newSchedule - Model schedule from the client along with reschedule comment
	 * @param comment - Comment for rescheduling
	 * @param scheduleId - Schedule id used to update the schedule
	 * @param candidateId - Candidate id used to reschedule
	 * @param date - Interview date
	 * @param time - Interview time
	 * 
	 * @return schedule - After rescheduling successfully
	 */
	Schedule reschedule(Schedule newSchedule, String comment, long scheduleId, long candidateId, String date, String time);
	
	/**
	 * Gets the schedules by status
	 * 
	 * @param status - Status of the schedule to view
	 * 
	 * @return schedules - List of schedule having the given status
	 * If the given status is not available, returns null
	 */
	List<Schedule> getSchedulesByStatus(ScheduleStatus status);
	
}